import pika
import uuid

QUEUE_NAME = 'recipe_01'


def main():
    consumer_id = str(uuid.uuid4())
    print("Starting consumer with consumer_id = " + consumer_id)
    url = "amqp://localhost:5672"
    params = pika.URLParameters(url)
    connection = pika.BlockingConnection(params)
    channel = connection.channel()
    channel.queue_declare(queue=QUEUE_NAME)

    def callback(ch, method, properties, body):
        print(consumer_id + " >>> started processing !!")
        # print(method)
        # print(properties)
        print(consumer_id + " >>> body ==> " + body.decode("utf-8"))
        print(consumer_id + " >>> finished processing !!")

    print(consumer_id + " >>> registering callback function !!!")
    channel.basic_consume(callback, queue=QUEUE_NAME, no_ack=True)
    print(consumer_id + " >>> waiting for messages !!")
    channel.start_consuming()
    print(consumer_id + " >>> closing consumer !!")
    connection.close()


if __name__ == '__main__':
    main()
